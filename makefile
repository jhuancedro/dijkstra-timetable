BUILD = build/

all: main run clean

main:
	@echo "COMPILANDO..."
	@g++ main.cpp */*.cpp -o ${BUILD}main
	@echo "OK"

run:
	@echo "EXECUTANDO..."
	@build/main
	@echo "FIM"

clean:
	@echo "LIMPANDO..."
	@rm ${BUILD}*
	@echo "OK"
