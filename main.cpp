#include <iostream>
#include "Lista/Lista.h"
#include "Grafo/Grafo.h"
#include "Horarios/Horarios.h"

using namespace std;

template <class T>
void testeLista(){
    cout << "\nTestando funcionalidades de Lista<T>" << endl;

    Lista<T> L;

    L.inserir(1);
    L.inserir(3);
    L.inserir(5);
    L.inserir(19);
    L.inserir(8);
    L.inserir(2);
    L.inserir(3);
    L.inserir(15);

    for (L.itInicio(); !L.itEhFim(); L.itProx()) {
        cout << L.getIt() << endl;
    }
}

void testeGrafo(){
    cout << "\nTestando funcionalidades basicas de Grafo" << endl;
    Grafo G;

    G.insereNo(1, 0, 0);
    G.insereNo(2, 0, 0);
    G.insereNo(3, 0, 0);
    G.insereNo(4, 0, 0);

    G.insereArco(1, 2, 0, 0);
    G.insereArco(2, 3, 1, 0);
    G.insereArco(3, 4, 2, 0);
    G.insereArco(1, 4, 3, 0);
    G.insereArco(4, 1, 4, 0);

    G.imprimir();
}

void testeDijkstra(){
    cout << "\nTestanto execucao do dijkstra." << endl;

    char arquivo[50] = "Instancias/instancia_exemplo.dtt";
    cout << "Instancia de teste: " << arquivo << endl;

    uint idOrigem, idDestino, h_saida;
    Grafo* G = Grafo::leituraInstanciaDijkstraTimetable(
            arquivo, idOrigem, idDestino, h_saida);
    char strSaida[5]; strHorario(h_saida, strSaida);

    cout << "\nGrafo obtido da entrada:" << endl;
    G->imprimir();

    cout << "\nDados do problema:" << endl;
    cout << "Origem: "  << idOrigem << endl;
    cout << "Destino: " << idDestino << endl;
    cout << "Horário de saída: " << strSaida << endl;


    cout << "\nSolução obtida com Dijkstra:" << endl;
    G->dijkstra(idOrigem, idDestino, h_saida);

    delete G;
}

int main() {
//    testeLista<float>();

//    testeGrafo();

    testeDijkstra();

    return 0;
}