#ifndef DIJKSTRA_TIMETABLE_ARCO_H
#define DIJKSTRA_TIMETABLE_ARCO_H

#include <cstdio>
#include "No.h"

class No;
typedef unsigned int uint;

class Arco{
private:
    uint id;
    No *noDestino;
    uint peso;
public:
    Arco(uint id, No* noDestino, uint peso);

    uint getId();
    No  *getNoDestino();
    uint getPeso();

    void imprimir();

    ~Arco();;
};


#endif //DIJKSTRA_TIMETABLE_ARCO_H
