#ifndef DIJKSTRA_TIMETABLE_GRAFO_H
#define DIJKSTRA_TIMETABLE_GRAFO_H

#include "Arco.h"
#include "No.h"
#include <iostream>
#include <fstream>

using namespace std;
typedef unsigned int uint;

class Grafo {
private:
    Lista<No*> nos;     // Lista de nos
    uint numeroNos;
    uint numeroArcos;

public:
    Grafo();
    uint getNumeroNos()  {    return numeroNos;     }
    uint getNumeroArcos(){    return numeroArcos;   }

    Arco * insereArco(No *noOrigem, No *noDestino, uint id, uint peso);
    Arco * insereArco(uint idOrigem, uint idDestino, uint id, uint peso);

    No *insereNo(uint id, uint min_fechamento, uint min_abertura);
    No* buscaNo(uint id);

    void desmarcaNos();
    void imprimir();

    void dijkstra(uint idOrigem, uint idDestino, uint saida);
    void dijkstra(No*  noOrigem, No*  noDestino, uint saida_real);

    static Grafo* leituraInstanciaDijkstraTimetable(const char *nomeArq, uint &idOrigem, uint &idDestino, uint &h_saida);

    ~Grafo();
};

#endif //DIJKSTRA_TIMETABLE_GRAFO_H
