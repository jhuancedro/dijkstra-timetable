#ifndef DIJKSTRA_TIMETABLE_NOGRAFO_H
#define DIJKSTRA_TIMETABLE_NOGRAFO_H

#include "Arco.h"
#include "../Lista/Lista.h"

typedef unsigned int uint;
class Arco;

class No {
private:
    Lista<Arco*> arcos;     // Lista de arcos
    uint min_fechamento, min_abertura;    // horarios de min_fechamento e min_abertura em minutos do dia
    uint id;
    bool marcado;           // Indica se o no esta marcado ou nao (flag)
public:
    No(uint id, uint min_fechamento, uint min_abertura);

    uint getID(){   return this->id;    };

    void marcar();
    void desmarcar();
    bool estahMarcado();

    Arco *insereArco(No *noDestino, uint id, uint peso);

    uint tempoAbertura(uint min_atual);

    /// Torna publica chamada dos metodos de iteracao da lista de arcos
    void arcosItInicio();
    void arcosItProx();
    bool arcosItEhFim();
    Arco* arcosGetIt();

    void imprimir();

    ~No();
};

#endif //DIJKSTRA_TIMETABLE_NOGRAFO_H
