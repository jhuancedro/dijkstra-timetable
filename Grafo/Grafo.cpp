#include <map>
#include "Grafo.h"
#include "../Horarios/Horarios.h"
#include <limits>

/** Constante de maior valor de unsigned int */
#define INFINITO numeric_limits<uint>::max()

using namespace std;

/** Construtor do grafo */
Grafo::Grafo(){
    numeroArcos = 0;
    numeroNos = 0;
}

/** Insercao de arco dados os nos o peso. Retorna o arco inserido. */
Arco * Grafo::insereArco(No *noOrigem, No *noDestino, uint id, uint peso) {
    this->numeroArcos++;
    return noOrigem->insereArco(noDestino, id, peso);
}

/** Insercao de arco dado os ids dos nos e peso. Retorna o arco inserido. */
Arco * Grafo::insereArco(uint idOrigem, uint idDestino, uint id, uint peso) {
    No *noOrigem  = buscaNo(idOrigem );
    No *noDestino = buscaNo(idDestino);
    return insereArco(noOrigem, noDestino, id, peso);
}

/** Desmarcar todos os nos do grafo */
void Grafo::desmarcaNos(){
    for(nos.itInicio(); !nos.itEhFim(); nos.itProx())
        nos.getIt()->desmarcar();
}

/** Impressao da estrutura do grafo */
void Grafo::imprimir() {
    cout << "Numero de nos: "  << numeroNos   <<
         "\tnumero de arcos: " << numeroArcos << endl;

    for(nos.itInicio(); !nos.itEhFim(); nos.itProx())
        nos.getIt()->imprimir();
}

/** Insere um No no grafo. Retorna o no inserido. */
No *Grafo::insereNo(uint id, uint min_fechamento, uint min_abertura) {
    No* no = new No(id, min_fechamento, min_abertura);
    nos.inserir(no);
    numeroNos++;

    return no;
}

/** Retorna o no que possui determinado id */
No *Grafo::buscaNo(uint id){
    for(nos.itInicio(); !nos.itEhFim(); nos.itProx()){
        if(nos.getIt()->getID() == id)
            return nos.getIt();
    }

    return NULL;
}

/** Retorna um grafo criado a partir de um arquivo de entrada */
Grafo* Grafo::leituraInstanciaDijkstraTimetable(const char *nomeArq, uint &idOrigem, uint &idDestino, uint &h_saida){
    ifstream entrada;
    entrada.open(nomeArq);

    uint numNos, numArcos, ni, nj, peso;
    uint h_fechamento_no, h_abertura_no;
    uint idNo, idArco;

    entrada >> numNos           // Numero de nos do grafo
            >> numArcos         // Numero de arcos do grafo
            >> idOrigem         // ID do no de origem  do problema
            >> idDestino        // ID do no de destino do problema
            >> h_saida;         // Horario de min_abertura do problema

    h_saida = converteEmMin(h_saida);

    Grafo* G = new Grafo();

    /// insercao de nos
    No* nos[numNos];
    for(uint i = 0; i < numNos; i++) {
        entrada >> idNo >> h_fechamento_no >> h_abertura_no;

        // os nos comecam com id = 1
        nos[i] = G->insereNo(i+1,
                             converteEmMin(h_fechamento_no),
                             converteEmMin(h_abertura_no));
    }

    /// insercao de arestas
    idArco = 1;
    while(idArco <= numArcos){
        entrada >> ni >> nj >> peso;
        if(ni > numNos || nj > numNos){
            cout << "ID dos nos maior que numero de nos!" << endl;
            delete G;
            entrada.close();
            return NULL;
        }

        // O peso eh dado em horas e armazenado em minutos
        G->insereArco(nos[ni - 1], nos[nj - 1], idArco++, 60*peso);
    }

    entrada.close();
    return G;
}

/** Encontra caminho minimo entre dois nos pelo algoritmo de Djkstra,
 *  RECEBENDO OS IDS dos nos origem e destino com o horario de saida */
void Grafo::dijkstra(uint idOrigem, uint idDestino, uint saida) {
    dijkstra(buscaNo(idOrigem), buscaNo(idDestino), saida);
}

/** Encontra caminho minimo entre dois nos pelo algoritmo de Djkstra,
 *  RECEBENCO OS NOS origem e destino com o horario de saida */
void Grafo::dijkstra(No *noOrigem, No *noDestino, uint saida) {
    this->desmarcaNos();

    map<uint, uint> id_pos;            /// mapeamento (id -> pos) nos vetores (distancias) e (naoVisitados)
    uint distancias[this->numeroNos];  /// distancia de noOrigem a todos os nos

    No* array_nos [this->numeroNos];     /// array de nos
    No* anteriores[this->numeroNos];     /// array de anteriores

    ///Inicializa conjuntos
    uint i = 0;
    for(nos.itInicio(); !nos.itEhFim(); nos.itProx(), i++){
        No *no = nos.getIt();
        // id_pos[no->getID()] = no->getID()-1;    /// mapeia ids com posicoes
        id_pos[no->getID()] = i;                                 /// mapeia ids com posicoes
        distancias[i] = INFINITO;     /// distancia "infinita"
        array_nos [i] = no;                                   /// array com todos os nos
        anteriores[i] = NULL;
    }

    /// Distancia para a origem
    distancias[ id_pos[noOrigem->getID()] ] = noOrigem->tempoAbertura(saida);


    No *noAtual = noOrigem, *noAdjacente;
    uint posNoMaisProx = 0;   // posicao do no desmarcado próximo
    /// Enquanto solucao nao estiver completa
    while(noAtual != NULL && noAtual != noDestino){
        noAtual->marcar();       // marca no como visitado

        /// Atualizar distancia de noOrigem ate os nos adjacentes a noAtual desmarcados
        uint distAteNoAtual = distancias[ id_pos[noAtual->getID()] ];
        uint distanciaPeloNoAtual;

        // para cada arco do no atual
        for(noAtual->arcosItInicio(); !noAtual->arcosItEhFim(); noAtual->arcosItProx()){
            noAdjacente = noAtual->arcosGetIt()->getNoDestino();
            if (!noAdjacente->estahMarcado()){
                // calculo de distancia de noOrigem ate noAdjacente passando por noAtual
                distanciaPeloNoAtual  = distAteNoAtual + noAtual->arcosGetIt()->getPeso();      // distancia atual +  peso do arco
                distanciaPeloNoAtual += noAdjacente->tempoAbertura(saida+distanciaPeloNoAtual); // tempo de espera para abertura do no

                if (distancias[ id_pos[noAdjacente->getID()] ] > distanciaPeloNoAtual){
                    distancias[ id_pos[noAdjacente->getID()] ] = distanciaPeloNoAtual;
                    anteriores[ id_pos[noAdjacente->getID()] ] = noAtual;
                }
            }
        }

        /// Busca no desmarcado com menor distancia de noOrigem
        for(i = 0; i < numeroNos && array_nos[i]->estahMarcado(); i++);     // busca primeiro no desmarcado
        posNoMaisProx = i;

        while (i < this->numeroNos) {               // verifica a posicao mais proxima com os demais nos desmarcados
            if (!array_nos[i]->estahMarcado() && distancias[i] < distancias[posNoMaisProx])
                posNoMaisProx = i;
            i++;
        }

        if (distancias[posNoMaisProx] == INFINITO)
            noAtual = NULL;     // Grafo eh desconexo
        else
            noAtual = array_nos[posNoMaisProx];
    }

    if (noAtual == NULL)
        printf("Nao ha caminho entre os nos %d e %d.\n", noOrigem->getID(), noDestino->getID());
    else {
        uint custo = distancias[id_pos[noDestino->getID()]];
        cout << "Custo: ";  imprimeTempo(custo);

        /// Constroi caminho da solucao
        No* noCaminho = noDestino;
        cout << "Caminho: \n\t";
        while (noCaminho != NULL) {
            cout << noCaminho->getID() << " <-- ";
            noCaminho = anteriores[ id_pos[noCaminho->getID()] ];
        }
        cout << endl;
    }
}


Grafo::~Grafo(){ }
