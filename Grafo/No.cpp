#include "No.h"
#include "../Horarios/Horarios.h"

No::No(uint id, uint min_fechamento, uint min_abertura) :
        id(id), marcado(false), min_fechamento(min_fechamento), min_abertura(min_abertura) {}

No::~No() {}

void No::marcar() { this->marcado = true; }

void No::desmarcar() { this->marcado = false; }

bool No::estahMarcado() { return this->marcado; }

Arco * No::insereArco(No *noDestino, uint id, uint peso) {
    Arco* novoArco = new Arco(id, noDestino, peso);
    arcos.inserir(novoArco);
    return novoArco;
}

void No::arcosItInicio() {  arcos.itInicio();   }

void No::arcosItProx()   {  arcos.itProx();     }

bool No::arcosItEhFim()  {  return arcos.itEhFim(); }

Arco *No::arcosGetIt()   {  return arcos.getIt();    }

/** Visualizacao ids e horarios de entrada e saida  */
void No::imprimir() {
    char ha[5], hf[5];
    strHorario(min_abertura  , ha);
    strHorario(min_fechamento, hf);

    printf("( N%02d, {%s, %s} ) ", id, hf, ha);

    for (arcos.itInicio(); !arcos.itEhFim(); arcos.itProx())
        arcos.getIt()->imprimir();

    cout << endl;
}

/** Retorna o tempo restante para o inicio do horario de chegada, a partir de tempoAtual */
uint No::tempoAbertura(uint min_atual) {
    // ignora dias no calculo da janela de tempo
    min_atual %= MINS_POR_DIA;

	// Caso a circulacao seja permitida
	if (min_fechamento <= min_abertura){
		if (min_atual < min_fechamento || min_atual > min_abertura)
			return 0;
	}else{
		if (!(min_atual < min_fechamento || min_atual > min_abertura))
			return 0;
	}

    // Caso a janela esteja fechada
    return minutosRestantes(min_atual, min_abertura);
}

