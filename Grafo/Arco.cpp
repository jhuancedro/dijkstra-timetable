#include "Arco.h"
#include "../Horarios/Horarios.h"

Arco::Arco(uint id, No *noDestino, uint peso)
        : id(id), noDestino(noDestino), peso(peso) { }

No *Arco::getNoDestino() { return this->noDestino; }

uint Arco::getId() {    return id;      }

uint Arco::getPeso() {  return peso;    }

/** Visualizacao de id e custo*/
void Arco::imprimir() {
    char hp[5];
    strHorario(peso, hp);
    printf("|--[A%02d, %s]--> (N%02d) ", id, hp, noDestino->getID());
}

Arco::~Arco() {}
