#ifndef DIJKSTRA_TIMETABLE_NOLISTA_H
#define DIJKSTRA_TIMETABLE_NOLISTA_H

#include <iostream>

using namespace std;

template <class T> class NoLista{
private:
    NoLista<T>* prox;
    T item;
public:
    NoLista(T obj) : prox(NULL), item(obj) {}

    NoLista *getProx() const {
        return prox;
    }

    void setProx(NoLista *prox) {
        this->prox = prox;
    }

    T getObj() const {
        return item;
    }

    void setObj(T obj) {
        this->item = obj;
    }

    bool ehFinal(){
        return prox == NULL;
    }

    ~NoLista() { }
};


#endif //DIJKSTRA_TIMETABLE_NOLISTA_H
