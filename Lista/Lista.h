#ifndef DIJKSTRA_TIMETABLE_LISTA_H
#define DIJKSTRA_TIMETABLE_LISTA_H

#include <iostream>
#include <cstdlib>

#include "NoLista.h"

using namespace std;
typedef unsigned int uint;

template<class T> class Lista{
private:
    NoLista<T> *noInicial, *it;
    uint qtdNos;
public:
    Lista() : noInicial(NULL), qtdNos(0) {};

    void inserir(T item);         // insere o objeto no inicio da Lista
    // void remover();           // remove o objeto apos o iterador da lista

    /// Metodos para iteracao na lista
    void itInicio();    // define iterador como primeiro no da lista
    void itProx();      // define o iterador para a próxima posicao da lista
    bool itEhFim();     // verifica se iterador estah no final da lista (NULL)
    T getIt();          // retorna o objeto apontado pelo iterador

    uint size(){ return qtdNos; }

    ~Lista();
};


template<class T>
void Lista<T>::inserir(T item){
    NoLista<T> *novoNo = new NoLista<T>(item);
    novoNo->setProx(noInicial);
    noInicial = novoNo;
    qtdNos++;
}

template<class T>
T Lista<T>::getIt(){
    return it->getObj();
}

template<class T>
void Lista<T>::itInicio(){
    this->it = noInicial;
}

template<class T>
void Lista<T>::itProx(){
    if(!itEhFim())
        it = it->getProx();
}

template<class T>
bool Lista<T>::itEhFim(){
    return it == NULL;
}

template<class T>
Lista<T>::~Lista(){
    itInicio();
    while(!itEhFim()){
        itProx();
        delete noInicial;
        noInicial = it;
    }
    noInicial = NULL;
}

#endif //DIJKSTRA_TIMETABLE_LISTA_H
