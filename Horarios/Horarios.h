#ifndef DIJKSTRA_TIMETABLE_HORARIOS_H
#define DIJKSTRA_TIMETABLE_HORARIOS_H

#define MINS_POR_DIA (60*24)

typedef unsigned int uint;

/** Converte formato hora-minuto em minutos deste as 00h00min
 *      exemplo: 0830 --> 08*60 + 30 = 510      */
uint converteEmMin(uint horario);

/** Calcula minutos restantes dos horarios em minutos minutosA e minutosB
 *
 *  Caso minutosA seja maior que minutosB, retorna a diferença de
 *  minutosA ate o horario do dia seguinte  */
uint minutosRestantes(uint minutosA, uint minutosB);

/** Imprime tempo no formato "XX dias, XX horas e XX minutos"*/
void imprimeTempo(uint minutos);

/** Salva em hora tempo no formato "XX:XX"*/
void strHorario(uint minutos, char hora[5]);

#endif //DIJKSTRA_TIMETABLE_HORARIOS_H
