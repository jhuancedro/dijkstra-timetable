#include <cstdio>
#include "Horarios.h"

/** Converte horario no formato "HHMM" para
 * quantidade de minutos a partir de 00:00 */
uint converteEmMin(uint horario) {
    return 60 * (horario / 100) + (horario % 100);
}

/** Verifica quantidade de minutos restantes de minutosA até minutosB,
 * seja no mesmo dia, seja no dia seguinte.*/
uint minutosRestantes(uint minutosA, uint minutosB) {
    if(minutosA <  minutosB)
        return minutosB - minutosA;     // diferenca simples e tempo

    else   // diferenca ate o horario do dia seguinte
        return (MINS_POR_DIA - minutosA) // tempo restante no dia atual
               + minutosB;             // tempo o dia seguinte
}

/** Imprime quantidade de minutos no formato
 * "XX dias, XX horas e XX minutos" */
void imprimeTempo(uint minutos){
    uint dias  =  minutos / MINS_POR_DIA;
    uint horas = (minutos % MINS_POR_DIA) / 60;
    uint min   = (minutos % MINS_POR_DIA) % 60;

    printf("%d dias, %d horas e %d minutos\n", dias, horas, min);
}

/** Salva quantidade de minutos no formato "XX:XX" em hora*/
void strHorario(uint minutos, char hora[5]) {
    uint horas = (minutos % MINS_POR_DIA) / 60;
    uint min   = (minutos % MINS_POR_DIA) % 60;

    sprintf(hora, "%02d:%02d", horas, min);
}
